package com.wh.common.response;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;

public class Response<T> {

    private int version = 1;

    private String requestId;

    @JsonFormat(pattern="yyyy-MM-dd hh:mm:ss")
    private LocalDateTime time;

    private int code;

    private boolean success;

    private String message;

    private T data;

    private Object dataExt;

    public Response(){}


    public Response(int code, String message, T data) {
        this.time = LocalDateTime.now();
        this.code = code;
        this.success = code == 200;
        this.message = message;
        this.data = data;
    }

    public Response(int code, Exception exception, T data) {
        this.time = LocalDateTime.now();
        this.code = code;
        this.success = code == 200;
        this.message = exception.getMessage();
        this.data = data;
    }

    public Response(ResponseCode venusResponseCode) {
        this.time = LocalDateTime.now();
        this.code = venusResponseCode.getCode();
        this.success = code == 200;
        this.message = venusResponseCode.getMessage();
    }

    public Response(ResponseCode venusResponseCode, T data) {
        this.time = LocalDateTime.now();
        this.code = venusResponseCode.getCode();
        this.success = code == 200;
        this.message = venusResponseCode.getMessage();
        this.data   = data;
    }
    public Response(ResponseCode venusResponseCode, String message) {
        this.time = LocalDateTime.now();
        this.code = venusResponseCode.getCode();
        this.success = code == 200;
        this.message = message;
    }

    public Response(ResponseCode venusResponseCode, Exception e) {
        this.time = LocalDateTime.now();
        this.code = venusResponseCode.getCode();
        this.success = code == 200;
        this.message = e.getMessage();
    }

    protected Response(int code, boolean success, String message, T data ) {
        this.time = LocalDateTime.now();
        this.code = code;
        this.success = success;
        this.message = message;
        this.data = data;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Object getDataExt() {
        return dataExt;
    }

    public void setDataExt(Object dataExt) {
        this.dataExt = dataExt;
    }
}
