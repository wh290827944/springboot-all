package com.wh.common.response;

public interface ResponseCode {

    int getCode();

    String getMessage();
}
