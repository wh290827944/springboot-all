package com.wh.common.exception;


import com.wh.common.response.Response;
import com.wh.common.response.ResponseHttpCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 异常处理类，用来处理request请求，响应时异常处理，用于Controller层
 */
@ControllerAdvice
public class ExceptionHand {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 返回对象 VenusResponse提示异常
     * @param e
     * @return
     */
    @ExceptionHandler({VenusResponseException.class})
    @ResponseBody
    public Response exception(VenusResponseException e) {
        e.printStackTrace();
        logger.error(">>>>>>>>>>>>>>>>>错误信息:{}", e.getMessage());
        return new Response(ResponseHttpCode.InternalServerError, e.getMessage());

    }




    /**
     * 捕捉运行时异常
     * @param e
     * @return
     */
    @ExceptionHandler({RuntimeException.class})
    @ResponseBody
    public Response exception(RuntimeException e) {
        e.printStackTrace();
        logger.error(">>>>>>>>>>>>>>>>>错误信息:{}", e.getMessage());
        return new Response(ResponseHttpCode.BadRequest, "运行出错，请联系系统管理员");

    }

    /**
     * 捕捉最终异常
     * @param e
     * @return
     */
    @ExceptionHandler({Exception.class})
    @ResponseBody
    public Response exception(Exception e) {
        e.printStackTrace();
        logger.error(">>>>>>>>>>>>>>>>>错误信息:{}", e.getMessage());
        return new Response(ResponseHttpCode.BadRequest, "系统出错，请联系系统管理员");

    }
 


}
