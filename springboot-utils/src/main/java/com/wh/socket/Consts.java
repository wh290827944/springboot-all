package com.wh.socket;

/**
 * @Author: WangHao
 * @Date: 2021/10/15/14:59
 * @Description:
 */
public class Consts {

    //服务ip
    public static final String ServerIP="127.0.0.1";
    //服务端口
    public static final int ServerPort=9092;

}
