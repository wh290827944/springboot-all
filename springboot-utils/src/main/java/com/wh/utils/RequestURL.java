package com.wh.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * 获取某个 城市的天气状况数据，数据格式是Json
 */
public class RequestURL {
    /**
     * 通过城市名称获取该城市的天气信息
     *
     * @return
     */
    public static JSONObject GetUrlContext(String initurl) {
        StringBuilder sb = new StringBuilder();
        ;
        try {
            URL url = new URL(initurl);
            URLConnection conn = url.openConnection();
            InputStream is = conn.getInputStream();
            InputStreamReader isr = new InputStreamReader(is, "utf-8"); // 设置读取流的编码格式，自定义编码
            BufferedReader reader = new BufferedReader(isr);
            String line = null;
            while ((line = reader.readLine()) != null)
                sb.append(line + " ");
            reader.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject dataOfJson = JSONObject.parseObject(sb.toString());
        if (dataOfJson == null || dataOfJson.size() == 0)
            return null;
        return dataOfJson;

    }


}