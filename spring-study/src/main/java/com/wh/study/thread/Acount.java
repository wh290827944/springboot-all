package com.wh.study.thread;

import lombok.Data;

/**
 * @Author: WangHao
 * @Date: 2021/08/25/10:24
 * @Description:
 */
@Data
public class Acount {

    public static String name ;
    public  static double money = 0;


    public  Acount(String name, double money) {
        this.name = name;
        this.money = money;
    }

    public static synchronized void add(double a){
//        synchronized (this){

            try {
                Thread.sleep(2000);
                money += a;
                System.out.println("存款后:"+money);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
//        }

    }

    public synchronized  static  void sub(double a){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        money-= a;
        System.out.println("取款后:"+money);
    }

    public static void main(String[] args) {
        Acount acount = new Acount("王浩",100);



        new Thread(()->{
            Acount.add(30);
            Acount.sub(40);
        }).start();

        new Thread(()->{
            Acount.sub(30);
//            Acount.add(60);
        }).start();



        try {
            Thread.sleep(10000);
            System.out.println(Acount.name+"余额:"+Acount.money);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
