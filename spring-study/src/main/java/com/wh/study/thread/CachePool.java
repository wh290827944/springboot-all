package com.wh.study.thread;

import com.wh.study.rabbitMQ.Provider;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
public class CachePool {
    public Object resource1 = new Object();

    public Object resource2 = new Object();

    private Runnable getThread(final int i) {
        return new Runnable() {
            @Override
            public void run() {
                setMesage("1号线程放入");
                Provider.sendMessage();
                System.out.println(getMessage());
              /*  try {
                    System.out.println("Thread1 is running");
                    synchronized (resource1) {
                        System.out.println("Thread1 lock resource1");
                        Thread.sleep(1000);//休眠2s等待线程2锁定资源2
                        synchronized (resource2) {
                            System.out.println("Thread1 lock resource2");
                        }
                        System.out.println("Thread1 release resource2");
                    }
                    System.out.println("Thread1 release resource1");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("Thread1 is stop");*/

            }
        };
    }

   static final ThreadLocal threadLocal   = new InheritableThreadLocal();
    private static void setMesage(String message){
        threadLocal.set(message);
    }

    private static String getMessage(){
        String o = (String) threadLocal.get();
        return o;
    }


    private Runnable getThread2(final int i) {
        return new Runnable() {
            @Override
            public void run() {
                try {
                    setMesage("2号线程放入");
                    Provider.sendMessage();
                    System.out.println(getMessage());
                }
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                /*try {
                    System.out.println("Thread2 is running");
                    synchronized (resource2) {
                        System.out.println("Thread2 lock resource2");
                        Thread.sleep(1000);//休眠2s等待线程1锁定资源1
                        synchronized (resource1) {
                            System.out.println("Thread2 lock resource1");
                        }
                        System.out.println("Thread2 release resource1");
                    }
                    System.out.println("Thread2 release resource2");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                System.out.println("Thread2 is stop");*/
            }
        };
    }


    public static void main(String args[]) {





        setMesage("main线程放入");



//

//        System.out.println(getInt());
//
        ExecutorService cachePool = Executors.newFixedThreadPool(2);

        CachePool cachePool1 = new CachePool();
        cachePool.execute(cachePool1.getThread(0));
        CachePool cachePool2 = new CachePool();
        cachePool.execute(cachePool1.getThread2(0));


        cachePool.shutdownNow();
        System.out.println(getMessage());
    }


    public static int getInt() {
        int a = 10;
        try {
            System.out.println(a / 0);
            a = 20;
        } catch (ArithmeticException e) {
            a = 30;
            return a;
        } finally {
            a = 40;

        }
        return a;
    }
}




