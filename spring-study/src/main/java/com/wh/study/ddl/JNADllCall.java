package com.wh.study.ddl;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.win32.StdCallLibrary;

/**
 * @Author: WangHao
 * @Date: 2021/08/27/17:34
 * @Description:
 */
public class JNADllCall {

    /**
     * DLL动态库调用方法1
     *
     * @Description: 读取调用StdCall方式导出的DLL动态库方法
     * @author: LinWenLi
     * @date: 2018年7月18日 上午10:37:58
     */
    public interface StdCallDll extends StdCallLibrary {
        // DLL文件默认路径为项目根目录，若DLL文件存放在项目外，请使用绝对路径
        StdCallDll INSTANCE = (StdCallDll) Native.loadLibrary(("TEST_DLL_2.dll"),
                StdCallDll.class);// 加载动态库文件
        // 声明将要调用的DLL中的方法（可以是多个方法）

        int add(int v1,int v2);
    }

    /**
     * DLL动态库调用方法2
     *
     * @Description: 读取调用Decl方式导出的DLL动态库方法
     * @author: LinWenLi
     * @date: 2018年7月18日 上午10:49:02
     */
    public interface CLibrary extends Library {
        CLibrary INSTANCE = (JNADllCall.CLibrary) Native.loadLibrary("TEST_DLL_2",
                CLibrary.class);

        // 声明将要调用的DLL中的方法（可以是多个方法）
        int add(int v1,int v2);
    }

    public static void main(String[] args) {

        try {
            CLibrary instance = CLibrary.INSTANCE;

            int add = CLibrary.INSTANCE.add(1, 2);
            System.out.println(add);
        }catch (Exception e ){
            e.printStackTrace();
        }



//        try {
//            int add = StdCallDll.INSTANCE.add(1, 2);
//            System.out.println(add);
//        }catch (Exception e ){
//            e.printStackTrace();
//        }



    }

}
