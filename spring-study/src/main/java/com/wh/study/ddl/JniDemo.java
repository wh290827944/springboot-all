package com.wh.study.ddl;

/**
 * @Author: WangHao
 * @Date: 2021/08/30/15:44
 * @Description:
 */
public class JniDemo {

    static{
        System.loadLibrary("spring-study/src/main/java/com/wh/study/ddl/TEST_DLL_2");
    }
    public native  int add(int a,int b);

    public static void main(String[] args) {
        JniDemo test = new JniDemo();
        int add = test.add(2, 3);
        System.out.println(add);

    }

}
