#ifndef TEST_DLL_2_H
#define TEST_DLL_2_H

#include "test_dll_2_global.h"

TEST_DLL_2SHARED_EXPORT int add(int v1,int v2);

class TEST_DLL_2SHARED_EXPORT TEST_DLL_2
{

public:
    TEST_DLL_2();

    double sub(double v1, double v2);
};

#endif // TEST_DLL_2_H
