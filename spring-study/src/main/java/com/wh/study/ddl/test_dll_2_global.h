#ifndef TEST_DLL_2_GLOBAL_H
#define TEST_DLL_2_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(TEST_DLL_2_LIBRARY)
#  define TEST_DLL_2SHARED_EXPORT Q_DECL_EXPORT
#else
#  define TEST_DLL_2SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // TEST_DLL_2_GLOBAL_H
