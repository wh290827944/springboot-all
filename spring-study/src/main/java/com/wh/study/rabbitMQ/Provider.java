package com.wh.study.rabbitMQ;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.MessageProperties;
import com.wh.study.rabbitMQ.utils.RabbitMQUtils;

import java.nio.charset.StandardCharsets;

public class Provider {

    public static void main(String[] args) throws Exception{
        sendMessage();
    }

    public static void sendMessage() {

        try {
            Connection connection = RabbitMQUtils.getConnection();
            //获取连接通道
            Channel channel = connection.createChannel();
            //通道绑定对应消息队列
            //arg1: 队列   arg2：是否持久化(重启MQ是否存在)  arg3：是否独占队列 arg4：是否在完成后自动删除队列  arg5：
            channel.queueDeclare("hello",true,false,false,null);

            //发布消息
            //MessageProperties.PERSISTENT_TEXT_PLAIN  消息持久化
            channel.basicPublish("","hello", MessageProperties.PERSISTENT_TEXT_PLAIN,"hello rabbitMQ".getBytes(StandardCharsets.UTF_8));


            System.out.println("消息发送成功");
            //关闭连接
            RabbitMQUtils.closeConnectionAndChanl(channel);
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
