package com.wh.study.rabbitMQ.fanoutQuene;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.wh.study.rabbitMQ.utils.RabbitMQUtils;

import java.nio.charset.StandardCharsets;

public class Provider {



    public static void main(String[] args) throws Exception{
        sendMessage();
    }

    public static void sendMessage() {

        try {
            Connection connection = RabbitMQUtils.getConnection();
            //获取连接通道
            Channel channel = connection.createChannel();
            //arg1 交换机名称  age2：交换机类型 fanout代表广播类型
            channel.exchangeDeclare("logs","fanout");

            //发布消息
            //MessageProperties.PERSISTENT_TEXT_PLAIN  消息持久化
            channel.basicPublish("logs","", null,"fanout type message".getBytes(StandardCharsets.UTF_8));


            System.out.println("消息发送成功");
            //关闭连接
            RabbitMQUtils.closeConnectionAndChanl(channel);
        }catch (Exception e){
            e.printStackTrace();
        }


    }
}
