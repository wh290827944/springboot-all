package com.wh.study.rabbitMQ.workQuene;

import com.rabbitmq.client.*;
import com.wh.study.rabbitMQ.utils.RabbitMQUtils;
import lombok.SneakyThrows;

import java.io.IOException;

public class Customer2 {

    public static void main(String[] args) throws IOException {
        getMessage();
    }

    public static void getMessage() throws IOException {
        Connection connection = RabbitMQUtils.getConnection();
        //获取连接通道
        Channel channel = connection.createChannel();
        channel.queueDeclare("work",true,false,false,null);
        channel.basicQos(1);//一次只接受一条未确认的消息
        // arg1:消费哪个消息队列  arg2：开始消息的自动确认机制   arg3：消费后的回调函数
        channel.basicConsume("work",false,new DefaultConsumer(channel){

            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                try {
                    Thread.sleep(1000);   //处理消息比较慢 一秒处理一个消息
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                System.out.println("消费者-2==========>"+new String(body));
                channel.basicAck(envelope.getDeliveryTag(),false);//手动确认消息
            }
        });




    }
}
