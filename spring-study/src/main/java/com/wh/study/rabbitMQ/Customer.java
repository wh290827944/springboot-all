package com.wh.study.rabbitMQ;

import com.rabbitmq.client.*;
import com.wh.study.rabbitMQ.utils.RabbitMQUtils;

import java.io.IOException;

public class Customer {

    public static void main(String[] args) throws Exception{
        getMessage();
    }

    public static void getMessage() throws Exception{
        Connection connection = RabbitMQUtils.getConnection();
        //获取连接通道
        Channel channel = connection.createChannel();
        channel.queueDeclare("hello",true,false,false,null);
        // arg1:消费哪个消息队列  arg2：开始消息的自动确认机制   arg3：消费后的回调函数
        channel.basicConsume("hello",true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("new String(body)==========>"+new String(body));
            }
        });




    }
}
