package com.wh.study.rabbitMQ.utils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


public class RabbitMQUtils {
    private final static ThreadLocal<Connection> threadLocal = new ThreadLocal<>();
    private static ConnectionFactory connectionFactory;

    static {
        connectionFactory = new ConnectionFactory();
        //设置连接主机
//        connectionFactory.setHost("192.168.85.11");
        connectionFactory.setHost("127.0.0.1");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("/ems");
        connectionFactory.setUsername("ems");
        connectionFactory.setPassword("123");



    }

    public static Connection getConnection() {
        try {
            Connection connection = connectionFactory.newConnection();
            threadLocal.set(connection);
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException();
        }

    }

    public static void closeConnectionAndChanl(Channel channel) {
        try {
            if (null != channel) channel.close();
            Connection connection = threadLocal.get();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
