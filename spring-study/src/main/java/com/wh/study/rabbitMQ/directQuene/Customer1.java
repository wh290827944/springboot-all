package com.wh.study.rabbitMQ.directQuene;

import com.rabbitmq.client.*;
import com.wh.study.rabbitMQ.utils.RabbitMQUtils;

import java.io.IOException;

public class Customer1 {

    public static void main(String[] args) throws Exception{
        getMessage();
    }

    public static void getMessage() throws Exception{
        Connection connection = RabbitMQUtils.getConnection();
        //获取连接通道
        Channel channel = connection.createChannel();
        //通道绑定交换机
        channel.exchangeDeclare("logs_direct","direct");
        //临时队列
        String queue = channel.queueDeclare().getQueue();
        //基于 rout key 绑定队列和交换机
        channel.queueBind(queue,"logs_direct","error");
        channel.basicConsume(queue,true,new DefaultConsumer(channel){
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                System.out.println("消费者-1==========>"+new String(body));
            }
        });


    }
}
