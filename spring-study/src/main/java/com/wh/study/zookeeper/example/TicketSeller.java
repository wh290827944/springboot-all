package com.wh.study.zookeeper.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TicketSeller {

    private void sell(){
        System.out.println("售票开始");
        // 线程随机休眠数毫秒，模拟现实中的费时操作
        int sleepMillis = 5000;
        try {
            //代表复杂逻辑执行了一段时间
            Thread.sleep(sleepMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("售票结束");
    }

    public void sellTicketWithLock() throws Exception {
        MyLock lock = new MyLock();
        // 获取锁
        lock.acquireLock();
        sell();
        //释放锁
        lock.releaseLock();
        System.out.println("*************************************************");
    }
    public static void main(String[] args) throws Exception {
//        TicketSeller ticketSeller = new TicketSeller();
//        for(int i=0;i<10;i++){
//            ticketSeller.sellTicketWithLock();
//        }

        //线程执行
        ExecutorService cachePool = Executors.newSingleThreadExecutor();
//        ExecutorService cachePool = Executors.newFixedThreadPool(4);
        for (int i = 0;i<5;i++){

            cachePool.execute(getThread());
        }

    }

    private static Runnable getThread() {
        return new Runnable() {
            @Override
            public void run() {
                TicketSeller ticketSeller = new TicketSeller();
                try {
                    ticketSeller.sellTicketWithLock();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }
}
