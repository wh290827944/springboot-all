package com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.order;


import com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.pizza.LDCheesePizza;
import com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.pizza.LDPepperPizza;
import com.wh.study.designPatterns.工厂模式.simplefactory.pizzastore.pizza.Pizza;

public class LDOrderPizza extends OrderPizza {

	
	@Override
    Pizza createPizza(String orderType) {
	
		Pizza pizza = null;
		if(orderType.equals("cheese")) {
			pizza = new LDCheesePizza();
		} else if (orderType.equals("pepper")) {
			pizza = new LDPepperPizza();
		}
		// TODO Auto-generated method stub
		return pizza;
	}

}
