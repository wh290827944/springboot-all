package com.wh.study.designPatterns.单例模式.枚举;

public class SingletonTest08 {
	public static void main(String[] args) {
		Singleton instance = Singleton.INSTANCE;
		Singleton instance2 = Singleton.INSTANCE;

		System.out.println(instance == instance2);
		
		System.out.println(instance.hashCode());
		System.out.println(instance2.hashCode());

		instance.sayOK();
	}
}

enum Singleton {
	INSTANCE("1"),
    TWO("2");
    private String id;

     Singleton(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void sayOK() {
        for (Singleton e : Singleton.values()) {
            String id = e.getId();
            System.out.println(id);
        }
        System.out.println("ok~");
	}



}