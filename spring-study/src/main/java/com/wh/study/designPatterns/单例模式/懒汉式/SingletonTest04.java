package com.wh.study.designPatterns.单例模式.懒汉式;


public class SingletonTest04 {

	public static void main(String[] args) {
		System.out.println("����ʽ2 �� �̰߳�ȫ~");
		Singleton2 instance = Singleton2.getInstance();
		Singleton2 instance2 = Singleton2.getInstance();
		System.out.println(instance == instance2); // true
		System.out.println("instance.hashCode=" + instance.hashCode());
		System.out.println("instance2.hashCode=" + instance2.hashCode());
	}

}

// ����ʽ(�̰߳�ȫ��ͬ������)
class Singleton2 {
	private static Singleton2 instance;
	
	private Singleton2() {}
	
	//�ṩһ����̬�Ĺ��з���������ͬ������Ĵ��룬����̰߳�ȫ����
	//������ʽ
	public static synchronized Singleton2 getInstance() {
		if(instance == null) {
			instance = new Singleton2();
		}
		return instance;
	}
}