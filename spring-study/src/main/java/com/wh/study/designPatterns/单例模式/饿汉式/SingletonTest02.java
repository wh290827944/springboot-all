package com.wh.study.designPatterns.单例模式.饿汉式;

public class SingletonTest02 {

	public static void main(String[] args) {
		//����
		Singleton2 instance = Singleton2.getInstance();
		Singleton2 instance2 = Singleton2.getInstance();
		System.out.println(instance == instance2); // true
		System.out.println("instance.hashCode=" + instance.hashCode());
		System.out.println("instance2.hashCode=" + instance2.hashCode());
	}

}

//����ʽ(��̬����)

class Singleton2 {
	
	//1. ������˽�л�, �ⲿ��new
	private Singleton2() {
		
	}
	

	//2.�����ڲ���������ʵ��
	private  static Singleton2 instance;
	
	static { // �ھ�̬������У�������������
		instance = new Singleton2();
	}
	
	//3. �ṩһ�����еľ�̬����������ʵ������
	public static Singleton2 getInstance() {
		return instance;
	}
	
}