package com.wh.study.designPatterns.单例模式.饿汉式;

public class SingletonTest01 {

	public static void main(String[] args) {
		//����
		Singleton2 instance = Singleton2.getInstance();
		Singleton2 instance2 = Singleton2.getInstance();
		System.out.println(instance == instance2); // true
		System.out.println("instance.hashCode=" + instance.hashCode());
		System.out.println("instance2.hashCode=" + instance2.hashCode());
	}

}

//����ʽ(��̬����)

class Singleton {
	
	//1. ������˽�л�, �ⲿ��new
	private Singleton() {
		
	}
	
	//2.�����ڲ���������ʵ��
	private final static Singleton instance = new Singleton();
	
	//3. �ṩһ�����еľ�̬����������ʵ������
	public static Singleton getInstance() {
		return instance;
	}
	
}