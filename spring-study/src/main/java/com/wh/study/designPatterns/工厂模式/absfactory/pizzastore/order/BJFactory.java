package com.wh.study.designPatterns.工厂模式.absfactory.pizzastore.order;


import com.wh.study.designPatterns.工厂模式.absfactory.pizzastore.pizza.BJCheesePizza;
import com.wh.study.designPatterns.工厂模式.absfactory.pizzastore.pizza.BJPepperPizza;
import com.wh.study.designPatterns.工厂模式.simplefactory.pizzastore.pizza.Pizza;

//���ǹ�������
public class BJFactory implements AbsFactory {

	@Override
	public Pizza createPizza(String orderType) {
		System.out.println("~ʹ�õ��ǳ��󹤳�ģʽ~");
		// TODO Auto-generated method stub
		Pizza pizza = null;
		if(orderType.equals("cheese")) {
			pizza = new BJCheesePizza();
		} else if (orderType.equals("pepper")){
			pizza = new BJPepperPizza();
		}
		return pizza;
	}

}
