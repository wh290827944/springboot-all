package com.wh.study.designPatterns.单例模式.懒汉式;


public class SingletonTest06 {

	public static void main(String[] args) {
		Singleton3 instance = Singleton3.getInstance();
		Singleton3 instance2 = Singleton3.getInstance();
		System.out.println(instance == instance2); // true
		System.out.println("instance.hashCode=" + instance.hashCode());
		System.out.println("instance2.hashCode=" + instance2.hashCode());
		
	}

}

class Singleton3 {
	private static volatile Singleton3 instance;
	
	private Singleton3() {}
	

	public static  Singleton3 getInstance() {
		if(instance == null) {
			synchronized (Singleton3.class) {
				if(instance == null) {
					instance = new Singleton3();
				}
			}
			
		}
		return instance;
	}
}