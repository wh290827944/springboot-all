package com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.order;


import com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.pizza.BJCheesePizza;
import com.wh.study.designPatterns.工厂模式.factorymethod.pizzastore.pizza.BJPepperPizza;
import com.wh.study.designPatterns.工厂模式.simplefactory.pizzastore.pizza.Pizza;

public class BJOrderPizza extends OrderPizza {

	
	@Override
    Pizza createPizza(String orderType) {
	
		Pizza pizza = null;
		if(orderType.equals("cheese")) {
			pizza = new BJCheesePizza();
		} else if (orderType.equals("pepper")) {
			pizza = new BJPepperPizza();
		}
		// TODO Auto-generated method stub
		return pizza;
	}

}
