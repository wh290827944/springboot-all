package com.wh.study.annotation.chat;

public class Test {

    public static void main(String[] args) {
        ChatType chatType = ChatFactory.getInstance(ChatType.chatMult);
        chatType.say();
        ChatType chatSingl = ChatFactory.getInstance(ChatType.chatSingl);
        chatSingl.say();
    }
}
