package com.wh.study.annotation.chat;

public interface ChatType {
    int chatSingl = 1;
    int chatMult = 2;
    void say();
}
