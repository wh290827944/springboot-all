package com.wh.study.annotation.chat;


import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringUtils implements ApplicationContextAware {
    private static ApplicationContext applicationContext = null;

    /**
     * 通过传递接口，返回接口实例
     *
     * @param cl
     * @param <T>
     * @return
     */
    public static <T> T getInterface(Class<T> cl) {
        T bean = null;
        if (null == applicationContext) {
            try {
                bean = cl.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        } else {
            bean = applicationContext.getBean(cl);// 注意是UserServiceI ， 不是UserServiceImpl
        }

        return bean;
    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (SpringUtils.applicationContext == null) {
            SpringUtils.applicationContext = applicationContext;
        }
    }

}
