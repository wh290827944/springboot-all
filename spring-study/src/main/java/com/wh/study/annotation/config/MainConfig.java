package com.wh.study.annotation.config;


import com.wh.study.annotation.bean.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MainConfig {
    @Bean("person01")
    public Person person(){
        return new Person("lisi",20);
    }


}
