package com.wh.study.annotation.chat;

import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Set;

public class ChatFactory {



    private static HashMap<Integer,String> sourceMap = new HashMap<>();

    /**
     * 单例
     *
     */
    static {
        System.out.println("工厂静态类执行了");

        //反射工具包，指明扫描路径
        Reflections reflections = new Reflections("com.wh.common.com.wh.study.annotation");
        //获取带我们pay注解的类
        Set<Class<?>> classSet =  reflections.getTypesAnnotatedWith(chat.class);
        //根据注解的值，将全类名放到map中
        for (Class clazz : classSet){
            chat pay = (chat) clazz.getAnnotation(chat.class);
            sourceMap.put(pay.value(),clazz.getCanonicalName());
        }

    }



    public static ChatType getInstance(int type){
        ChatType chatType = null;
        try {
            //取得全类名
            String className = sourceMap.get(type);
            //取得类对象
            Class  clazz= Class.forName(className);



            //======================创建对象=====================
            chatType  =  (ChatType) SpringUtils.getInterface(clazz);

        }catch (Exception e){
            e.printStackTrace();
            throw  new RuntimeException(e);
        }

        return chatType;
    }







}
