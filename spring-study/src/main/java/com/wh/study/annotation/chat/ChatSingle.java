package com.wh.study.annotation.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@chat( ChatType.chatSingl)
@Component //交给spring托管
public class ChatSingle implements ChatType {

    @Autowired
    ChatConfig chatConfig;

    @Override
    public void say() {

        if (null != chatConfig){
            System.out.println(chatConfig.getInfo());
        }

        System.out.println("这是单人聊天");

    }

}
