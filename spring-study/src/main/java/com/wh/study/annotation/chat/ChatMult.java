package com.wh.study.annotation.chat;

import org.springframework.stereotype.Component;

@chat( ChatType.chatMult)
@Component  //交给spring托管
public class ChatMult implements ChatType {

    @Override
    public void say() {
        System.out.println("这是多人聊天");

    }

}
