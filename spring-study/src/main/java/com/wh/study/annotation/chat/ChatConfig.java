package com.wh.study.annotation.chat;


import org.springframework.stereotype.Component;

@Component
public class ChatConfig {
    private String info = "聊天配置数据加载成功";

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
