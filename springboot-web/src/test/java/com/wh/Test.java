package com.wh;


import com.wh.dao.TestMapper;
import com.wh.entity.User;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith( SpringRunner.class)
@SpringBootTest
public class Test {

    @Autowired
    private TestMapper testMapper;

    @org.junit.Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
//        List<User> userList = testMapper.selectList(null);
        User userName = testMapper.getUserName();
        System.out.println(userName);
    }

}
