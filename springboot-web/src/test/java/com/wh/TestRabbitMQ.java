package com.wh;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith( SpringRunner.class )
@SpringBootTest( classes = WebApp.class )
public class TestRabbitMQ {
    @Autowired
    RabbitTemplate rabbitTemplate;

    //hello word 模型
    @Test
    public void test() {
        rabbitTemplate.convertAndSend("hello", "hello world");
    }

    //work模型
    @Test
    public void testWork() {
        for (int i = 0; i < 10; i++) {
            rabbitTemplate.convertAndSend("work", "work模型");

        }
    }

    //广播模型
    @Test
    public void testFanout() {
        rabbitTemplate.convertAndSend("logs", "", "广播模型");

    }

    //路由模型
    @Test
    public void testRout() {
        rabbitTemplate.convertAndSend("directs", "error", "路由《info》模型");

    }

    //主题模型
    @Test
    public void testTopic() {
        rabbitTemplate.convertAndSend("topics", "order.id", "主题模型");

    }
}
