package com.wh.entity;

import lombok.Data;

/**
 * 文件表(iot_file)
 * 
 * @author wh
 * @version 1.0.0 2021-07-13
 */
@Data
public class IotFileDO implements java.io.Serializable {
    /** 版本号 */
    private static final long serialVersionUID = 7699765967089006970L;

    /** 文件唯一标志 */
    private Integer fileId;

    /** 文件名 */
    private String fileName;

    /**文件唯一名
     * 通过算法生成的18位唯一数字+文件后缀名生成的唯一名字
     * */
    private String fileOnlyname;

    /** 文件地址 */
    private String fileUrl;
    /** 文件存储路径 */
    private String filePath;

    /** 文件后缀名 */
    private String fileSuffix;

    /** 文件大小 */
    private Long fileSize;

    /** 文件状态 */
    private Integer fileStatus;

    /** 文件类型：“图片”、“模型”等等 */
    private String fileType;

    /** 容器唯一标志 */
    private String containerId;

}