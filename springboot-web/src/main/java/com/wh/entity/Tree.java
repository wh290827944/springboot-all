package com.wh.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: WangHao
 * @Date: 2021/08/24/14:15
 * @Description:
 */
@Data
@Accessors(chain = true)
public class Tree {
    private int id;
    private String name;
    private List<Tree> childrens = new ArrayList<>();

    public Tree() {
    }

    public Tree(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
