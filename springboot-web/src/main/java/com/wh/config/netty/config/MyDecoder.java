package com.wh.config.netty.config;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

import java.util.List;

/**
 * @Author: WangHao
 * @Date: 2021/10/19/16:47
 * @Description:
 */
public class MyDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        String HEXES = "0123456789ABCDEF";
        byte[] req = new byte[msg.readableBytes()];
        msg.readBytes(req);
        final StringBuilder hex = new StringBuilder(2 * req.length);

        for (int i = 0; i < req.length; i++) {
            byte b = req[i];
            hex.append(HEXES.charAt((b & 0xF0) >> 4))
                    .append(HEXES.charAt((b & 0x0F))).append(" ");
        }
        System.out.println("原始报文-------" +
                ctx.channel().remoteAddress().toString()+ "----" + hex);

        String result = hex.toString().replaceAll("(?i)7D 02", "7E")
                .replaceAll("(?i)7D 01", "7D");


        out.add(result.replace(" ",""));
    }
}