package com.wh.config.netty.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "parse")
@Data
public class ParseConfiguration {
    public Integer port;
}
