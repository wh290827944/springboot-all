package com.wh.config.netty.config;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ChannelHandler.Sharable
public class ServerHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {

        Channel incoming = ctx.channel();
        String body = (String)msg;
        System.out.println("原始报文-------" +
                incoming.remoteAddress().toString()+ "----" + body);


        MyTools myTools=new MyTools();
        String substring = body.substring(2, 6);
        if ("0102".equalsIgnoreCase(substring)){
//            myTools.writeToClient("7E800100050143053805440000025B010200E07E",ctx,"测试");
        }

//        long dec_num = Long.parseLong(body, 16);



//        ByteBuf in = (ByteBuf) msg;
//        String readStr = bytesToHexString(in);

//        String readStr = in.toString(CharsetUtil.US_ASCII);

//        ReferenceCountUtil.release(msg);
//        byte[] bytes = msg.toString().getBytes();
//        String readStr = bytesToHexString(bytes);
//        String format = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
//        System.out.println(format+":"+ctx.channel().remoteAddress()+":客户端发送的信息...."+strTo16(msg.toString()));
//        ctx.write("Server write"+msg);
//        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }

    public static String strTo16(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch);
            str = str + s4;
        }
        return str;
    }




    public static byte[] hexStrToBytes(String str) {
        if (str == null) {
            return null;
        }
        str = str.replaceAll("\\s", "");
        if (str.length() == 0) {
            return new byte[0];
        }
        byte[] byteArray = new byte[str.length() / 2];
        for (int i = 0; i < byteArray.length; i++) {
            String subStr = str.substring(2 * i, 2 * i + 2);
            byteArray[i] = ((byte) Integer.parseInt(subStr, 16));
        }
        return byteArray;
    }

    public static String bytesToHexString(ByteBuf buffer) {
        if (null == buffer) {
            return "";
        }
        StringBuilder hexString = new StringBuilder();
        while (buffer.isReadable()) {
            hexString.append(String.format("%02x", buffer.readByte()));
        }
        return hexString.toString();
    }
}