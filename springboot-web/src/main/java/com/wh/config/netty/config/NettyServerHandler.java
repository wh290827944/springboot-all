package com.wh.config.netty.config;

import com.wh.config.gps.GpsProtocolDataHandler;
import io.netty.channel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.SocketAddress;


@Component
@ChannelHandler.Sharable
public class NettyServerHandler extends ChannelInboundHandlerAdapter {

    @Autowired
    ParseConfiguration parseConfiguration;
    @Autowired
    GpsProtocolDataHandler gpsProtocolDataHandler;

    /**
     * 读取客户端发送来的数据
     *
     * @param ctx ChannelHandler的上下文对象 有管道 pipeline 通道 channel 和 请求地址 等信息
     * @param msg 客户端发送的具体数据
     */
    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
//

        Channel incoming = ctx.channel();
        String body = (String) msg;
        String name = ctx.name();

        System.out.println("处理报文-------" +name+"----"+
                incoming.remoteAddress().toString() + "----" + body);

//        GpsProtocolDataHandler gpsProtocolDataHandler = new GpsProtocolDataHandler();
        gpsProtocolDataHandler.handleProtocol(ctx, body);

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }


    @Override
    public void channelRegistered(ChannelHandlerContext ctx) throws InterruptedException {
        SocketAddress address = ctx.channel().remoteAddress();
        System.out.println("客户端请求到了..." + address);
    }

    @Override
    public void channelUnregistered(ChannelHandlerContext ctx) throws Exception {
        SocketAddress address = ctx.channel().remoteAddress();
        System.out.println("客户端请求断开..." + address);
    }

    /**
     * 读取客户端发送数据完成后的方法
     * 在本方法中可以发送返回的数据
     *
     * @param ctx
     */
    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {


//        ctx.writeAndFlush(hexStrToBytes("ab13"));
        // writeAndFlush 是组合方法
//        ctx.channel().writeAndFlush(hexStrToBytes("ab13"));
//        ctx.writeAndFlush(Unpooled.copiedBuffer(rMsg,CharsetUtil.UTF_8));
    }


}
