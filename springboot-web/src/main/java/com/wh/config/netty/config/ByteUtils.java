package com.wh.config.netty.config;

/**
 * @Author: WangHao
 * @Date: 2021/10/20/10:08
 * @Description:
 */
public class ByteUtils {

    public static void main(String[] args) {
        int a = 34534;
        String s = DecToHexString(a,2);
        System.out.println(s);

        int i = HexStringToDec(s);
        System.out.println(i);

        int i1 = HexStringToDec("ff");
        System.out.println(i1);
    }

    /**
     * 十进制转十六进制字符
     * @param num
     * @param needLength
     * @return
     */
    public static String DecToHexString(Integer num, Integer needLength ) {
        String hex = num.toHexString(num);
        if (null != needLength){
            return addZeroForNum(hex,needLength);
        }
        return hex;
    }

    /**
     * 字符串长度补0
     * @param str
     * @param strLength
     * @Author: WangHao
     */
    public static String addZeroForNum(String str, int strLength) {
        int strLen = str.length();
        if (strLen < strLength) {
            while (strLen < strLength) {
                StringBuffer sb = new StringBuffer();
                sb.append("0").append(str);// 左补0
                // sb.append(str).append("0");//右补0
                str = sb.toString();
                strLen = str.length();

            }

        }
        return str;
    }


    /**
     * 十六进制字符转十进制
     * @param content
     * @return
     */
    public static int HexStringToDec(String content){
        if ("0X".equalsIgnoreCase(content.substring(0,2))){
            return Integer.parseInt(content.substring(2), 16);
        }else {
            return Integer.parseInt(content, 16);
        }

    }

    /**
     * byte数组转hex
     *
     * @param bytes
     * @return
     */
    public static String bytesToHexString(byte[] bytes) {
        String strHex = "";
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < bytes.length; n++) {
            strHex = Integer.toHexString(bytes[n] & 0xFF);
            sb.append((strHex.length() == 1) ? "0" + strHex : strHex); // 每个字节由两个字符表示，位数不够，高位补0
        }
        return sb.toString().trim();
    }

    /**
     * hex数组转byte
     * @param src
     * @return
     */
    public static byte[] hexString2Bytes(String src) {
        int l = src.length() / 2;
        byte[] ret = new byte[l];
        for (int i = 0; i < l; i++) {
            ret[i] = (byte) Integer
                    .valueOf(src.substring(i * 2, i * 2 + 2), 16).byteValue();
        }
        return ret;
    }




    /**
     * 异或运算和
     *
     * @param bytes
     * @return
     */
    public static byte[] byteOrbyte(byte[] bytes) {
        byte[] orbyte = new byte[1];
        byte value = bytes[0];
        for (int i = 1; i < bytes.length; i++) {
            value = (byte) (value ^ bytes[i]);
        }
        orbyte[0] = value;
        return orbyte;
    }
}
