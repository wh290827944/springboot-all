package com.wh.config.netty.config;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 中信报文解码器
 *
 * @Author DJ.Wang
 * @DateTime: 2020/8/17 15:44
 **/
@Slf4j
public class TransMsgDecoder extends ByteToMessageDecoder {

    /**
     * 报文长度头8
     */
    public final int BASE_LENGTH = 8;



    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        // 可读长度必须大于基本长度,否则继续接收
        if (byteBuf.readableBytes() >= BASE_LENGTH) {
            // 防止socket字节流攻击
            // 防止，客户端传来的数据过大
            // 因为，太大的数据，是不合理的;拆包粘包错误丢弃,不可大于4M
            //TODO 经测试一次decode最大1024字节
            log.info("可读大小[{}]", byteBuf.readableBytes());
            if (byteBuf.readableBytes() > 1024 * 1024 * 4) {
                log.error("包过大丢弃,[{}]",byteBuf.readableBytes());
                byteBuf.skipBytes(byteBuf.readableBytes());
                return;
            }

            int beginIndex = byteBuf.readerIndex();

            byte[] len = new byte[8];
            byteBuf.readBytes(len, 0, 8);
            int contentLen = Integer.valueOf(new String(len, "UTF-8"));

            byte[] content = new byte[contentLen];
            log.info("可读长度[{}]，还剩长度[{}]", byteBuf.readableBytes(), contentLen);
            if(byteBuf.readableBytes() < contentLen) {
                //重置起始读
                byteBuf.readerIndex(beginIndex);
                return;
            }

            byteBuf.readBytes(content, 0, contentLen);
            String contentStr = new String(content, "UTF-8");
            list.add(contentStr);
        }

    }

}
