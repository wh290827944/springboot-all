package com.wh.config.fileupload;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Author: WangHao
 * @Date: 2021/07/15/11:33
 * @Description:
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    //说明 : 启动类中   @SpringBootApplication(exclude = {MultipartAutoConfiguration.class})

    /**
     * 指定自定义解析器
     * 将 multipartResolver 指向我们刚刚创建好的继承 CustomMultipartResolver 类的 自定义文件上传处理类
     *
     * @return
     */
    @Bean(name = "multipartResolver")
    public MultipartResolver multipartResolver() {
        return new CustomMultipartResolver();
    }
}