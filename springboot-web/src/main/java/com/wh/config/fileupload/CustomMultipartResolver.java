package com.wh.config.fileupload;

import lombok.SneakyThrows;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author: WangHao
 * @Date: 2021/07/15/11:32
 * @Description:
 */
public class CustomMultipartResolver extends CommonsMultipartResolver {

    /**
     * 注入第二步写的FileUploadProgressListener
     * 此处一定要检查是否注入成功了，不成功的话会报错
     */
    @Autowired
    private FileUploadProgressListener progressListener;

    public void setProgressListener(FileUploadProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    @SneakyThrows
    @Override
    protected MultipartParsingResult parseRequest(HttpServletRequest request) {

        request.getSession().setAttribute("progressEntity",null);

        String encoding = determineEncoding(request);
        FileUpload fileUpload = super.prepareFileUpload(encoding);
//        fileUpload.setFileSizeMax(1024 * 1024 * 5000);// 单个文件最大500M
//        fileUpload.setSizeMax(1024 * 1024 * 5000);// 一次提交总文件最大500M
        //向文件上傳進度監視器設置session用於存儲上傳進度
        progressListener.setSession(request.getSession());
        //將文件上傳進度監視器加入到fileUpload中
        fileUpload.setProgressListener(progressListener);

        try {
            List<FileItem> fileItems = ((ServletFileUpload) fileUpload).parseRequest(request);
            return parseFileItems(fileItems, encoding);
        } catch (FileUploadException e) {
            throw new Exception(e);
        }
    }

}