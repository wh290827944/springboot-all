package com.wh.config.fileupload;

import java.io.File;

/**
 * @Author: WangHao
 * @Date: 2021/06/29/16:16
 * @Description:
 */
public interface UploadConstant {

     String resource = File.separator + "resources"+File.separator+"equipmentTwin"+File.separator;

     String equipmentTwinPath =  System.getProperty("user.dir") + File.separator + "resources"+File.separator+"equipmentTwin"+File.separator ;

//     String equipmentTwinModelPath =  System.getProperty("user.dir") + File.separator + "resources"+File.separator+"equipmentTwin"+File.separator ;;

}
