package com.wh.config.gps.handle;

import com.wh.config.gps.common.GpsContant;
import com.wh.config.gps.domain.GpsProtocol;
import com.wh.config.gps.sign.Sign;
import com.wh.config.netty.config.MyTools;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:38
 * @Description:
 */
@Component
@Sign(value = "0002",message = "终端心跳")
public class Sign0002 implements ISign {
    @Override
    public void handle(ChannelHandlerContext ctx, GpsProtocol gpsProtocol) throws Exception {
        String deviceCode = gpsProtocol.getDeviceCode();
        System.out.println("设备心跳数据上传====>"+deviceCode);
    }
}
