package com.wh.config.gps.domain;

import com.wh.config.gps.GpsProtocolDataHandler;
import com.wh.config.gps.utils.GpsUtils;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Author: WangHao
 * @Date: 2021/10/20/10:30
 * @Description:
 */
@Data
@Accessors(chain = true)
public class GpsProtocol {

    private String rawData;
    private String biaoZhiWei; // BYTE
    private String msgId;  //WORD
    private String msgAttr; //WORD
    private String deviceCode;  //BCD[6]
    private String serialNum; //WORD
    private String head;
    private String body; //String
    private String checkCode; //BYTE


    public GpsProtocol() {
    }

    public GpsProtocol(String biaoZhiWei, String msgId,  String deviceCode, String serialNum, String body) {
        this.biaoZhiWei = biaoZhiWei;
        this.msgId = msgId;
        this.deviceCode = deviceCode;
        this.serialNum = serialNum;
        this.body = body;
        this.msgAttr = GpsProtocolDataHandler.createMsgAttri(body);
        this.head = this.msgId+this.msgAttr+this.deviceCode+this.serialNum;
        this.checkCode = GpsProtocolDataHandler.createCheckCode(this.head+this.body);
    }

    public String getHexString() {
        return new StringBuilder().append(biaoZhiWei).append(msgId).append(msgAttr).append(deviceCode)
                .append(serialNum).append(body).append(checkCode).append(biaoZhiWei).toString();
    }


}
