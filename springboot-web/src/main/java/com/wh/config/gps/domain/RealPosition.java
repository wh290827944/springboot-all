package com.wh.config.gps.domain;

import com.wh.config.netty.config.ByteUtils;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/10:52
 * @Description:
 */
@Data
@Accessors(chain = true)
public class RealPosition implements Serializable {


    /**设备号码*/
    private String deviceCode;  //BCD[6]
    /**
     * 报警标志
     */
    private String alarmStatus;

    private String status;
    /**
     * 纬度
     */
    private Double lat;
    /**
     * 经度
     */
    private Double lon;
    /**
     * 海拔高度，单位为米（m）
     */
    private Integer altitude;
    /**
     * 速度  1/10km/h
     */
    private Integer speed;
    /**
     * 方向  0-359，正北为 0，顺时针
     */
    private Integer direction;
    /**
     * YYMMDDHHmmss（GMT+8 时间，本标准中之后涉及的时间均采用此时区)
     */
    private String time;

    public RealPosition() {
    }

    public RealPosition(RawPosition rawPosition) {
        if (null == rawPosition) return;
        this.deviceCode = rawPosition.getDeviceCode();
        this.alarmStatus = rawPosition.getAlarmStatus();
        this.status = rawPosition.getStatus();

        this.lat =  ByteUtils.HexStringToDec(rawPosition.getLat())/Math.pow(10,6);
        this.lon =  ByteUtils.HexStringToDec(rawPosition.getLon())/Math.pow(10,6);
        this.altitude = ByteUtils.HexStringToDec(rawPosition.getAltitude());
        this.speed = ByteUtils.HexStringToDec(rawPosition.getSpeed());
        this.direction = ByteUtils.HexStringToDec(rawPosition.getDirection());
        LocalDateTime parse = LocalDateTime.parse(rawPosition.getTime(),DateTimeFormatter.ofPattern("yyMMddHHmmss"));
        this.time = parse.format(DateTimeFormatter.ofPattern("yy-MM-dd HH:mm:ss"));
//        this.time = rawPosition.getTime();
    }

    public static void main(String[] args) {

        LocalDateTime parse = LocalDateTime.parse("211020071553",DateTimeFormatter.ofPattern("yyMMddHHmmss"));
        System.out.println(parse);
//        String lat = "01E91AC0";
//        double v = ByteUtils.HexStringToDec(lat) / Math.pow(10, 6);
//        System.out.println(v);
    }
}
