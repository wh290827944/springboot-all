package com.wh.config.gps.handle;

import com.wh.config.gps.domain.GpsProtocol;
import com.wh.config.gps.domain.RawPosition;
import com.wh.config.gps.domain.RealPosition;
import com.wh.config.gps.sign.Sign;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:38
 * @Description:
 */
@Component
@Sign(value = "0200",message = "位置信息汇报")
public class Sign0200 implements ISign {
    @Override
    public void handle(ChannelHandlerContext ctx, GpsProtocol gpsProtocol) throws Exception {
        RawPosition rawPosition = new RawPosition();
        if (null == gpsProtocol || null == gpsProtocol.getBody() || gpsProtocol.getBody().length()< 56) throw new Exception("do not hava this protocol message!");
        String positionBody = gpsProtocol.getBody();

        rawPosition.setDeviceCode( gpsProtocol.getDeviceCode())

                .setAlarmStatus(positionBody.substring(0,8))
                .setStatus(positionBody.substring(8,16))
                .setLat(positionBody.substring(16,24))
                .setLon( positionBody.substring(24,32))
                .setAltitude(positionBody.substring(32,36))
                .setSpeed(positionBody.substring(36,40))
                .setDirection(positionBody.substring(40,44))
                .setTime( positionBody.substring(44,56))
        ;

        RealPosition realPosition = new RealPosition(rawPosition);

        System.out.println("当前位置信息========>："+realPosition.toString());
    }
}
