package com.wh.config.gps;

import com.wh.config.gps.common.GpsContant;
import com.wh.config.gps.domain.GpsProtocol;
import com.wh.config.gps.handle.ISign;
import com.wh.config.gps.sign.SignFactory;
import com.wh.config.gps.utils.GpsUtils;
import com.wh.config.netty.config.ByteUtils;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/10:05
 * @Description: Gps协议数据解析类
 */
@Component
public class GpsProtocolDataHandler {

    /**
     * Gps
     * @param ctx
     * @param oriMsg
     */
    public void handleProtocol(ChannelHandlerContext ctx, String oriMsg) {
        try {
            GpsProtocol gpsProtocol = handleGpsProtocol(oriMsg);
            if (null == gpsProtocol) return;

            ISign instance = SignFactory.getInstance(gpsProtocol);
            if (null != instance) {
                instance.handle(ctx, gpsProtocol);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static GpsProtocol handleGpsProtocol(String oriData) throws Exception {
        if (oriData == null || oriData.length() < 30 || !oriData.toLowerCase(Locale.CHINA).startsWith("7e") || !oriData.toLowerCase(Locale.CHINA).endsWith("7e")) {
            System.out.println("不匹配协议:"+oriData);
            return null;
        }
        GpsProtocol gpsProtocol = new GpsProtocol();
        gpsProtocol.setRawData(oriData).setBiaoZhiWei(oriData.substring(0, 2))
                .setMsgId(oriData.substring(2, 6))
                .setMsgAttr(oriData.substring(6, 10))
                .setDeviceCode(oriData.substring(10, 22))
                .setSerialNum(oriData.substring(22, 26))
                .setBody(oriData.substring(26, oriData.length() - 4));
        ;

        return gpsProtocol;
    }

    public static String createMsgAttri(String body) {
        int i = body.length() / 2;
        String s = ByteUtils.DecToHexString(i,4);
        return s;
    }

    public static String createCheckCode(String headAndBody) {
        byte[] bytes = ByteUtils.hexString2Bytes(headAndBody);
        byte[] bytes1 = ByteUtils.byteOrbyte(bytes);
        String s = ByteUtils.bytesToHexString(bytes1);
        return s;
    }

    public static Double handleLatAndLonByHexString(String hexLatOrLon){

        return null;
    }
}
