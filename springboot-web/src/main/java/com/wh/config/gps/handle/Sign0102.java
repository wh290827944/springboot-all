package com.wh.config.gps.handle;

import com.wh.config.netty.config.MyTools;
import com.wh.config.gps.common.GpsContant;
import com.wh.config.gps.domain.GpsProtocol;
import com.wh.config.gps.sign.Sign;
import io.netty.channel.ChannelHandlerContext;
import org.springframework.stereotype.Component;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:38
 * @Description:
 */
@Component
@Sign(value = "0102",message = "终端鉴权")
public class Sign0102 implements ISign {
    @Override
    public void handle(ChannelHandlerContext ctx, GpsProtocol gpsProtocol) throws Exception {
        String sendBody = gpsProtocol.getSerialNum()+gpsProtocol.getMsgId()+"00";

        GpsProtocol sendProtocal = new GpsProtocol(gpsProtocol.getBiaoZhiWei(), GpsContant.msId_8001,
                gpsProtocol.getDeviceCode(),"0000",sendBody);

        String hexString = sendProtocal.getHexString();


        MyTools myTools =new MyTools();
//                myTools.writeToClient("7E800100050143053805440000025B010200E07E",ctx,"测试");
        myTools.writeToClient(hexString,ctx,"测试");
    }
}
