package com.wh.config.gps.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/10:17
 * @Description:
 */
@Data
@Accessors(chain = true)
public class RawPosition implements Serializable {
    /**设备号码*/
    private String deviceCode;  //BCD[6]
    /** 数据项个数*/
    private String dataCount;
    /** 位置数据类型*/
    private String positionType;
    /** 位置汇报数据体*/
    private String contentLength;
    /**
     * 报警标志
     */
    private String alarmStatus;
    private String status;
    /**
     * 纬度
     */
    private String lat;
    /**
     * 经度
     */
    private String lon;
    /**
     * 高程
     */
    private String altitude;
    /**
     * 速度
     */
    private String speed;
    /**
     * 方向
     */
    private String direction;

    private String time;


    public RawPosition() throws Exception {

    }

    @Deprecated
    public RawPosition(GpsProtocol gpsProtocol) throws Exception {
        if (null == gpsProtocol || null == gpsProtocol.getBody() || gpsProtocol.getBody().length()< 66) throw new Exception("do not hava this protocol message!");
        String positionBody = gpsProtocol.getBody();

        this.deviceCode = gpsProtocol.getDeviceCode();
        this.dataCount = positionBody.substring(0,4);
        this.positionType = positionBody.substring(4,6);
        this.contentLength = positionBody.substring(6,10);
        this.alarmStatus = positionBody.substring(10,18);
        this.status = positionBody.substring(18,26);
        this.lat = positionBody.substring(26,34);
        this.lon = positionBody.substring(34,42);
        this.altitude = positionBody.substring(42,46);
        this.speed = positionBody.substring(46,50);
        this.direction = positionBody.substring(50,54);
        this.time = positionBody.substring(54,60);

    }

}
