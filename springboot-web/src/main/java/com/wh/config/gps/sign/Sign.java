package com.wh.config.gps.sign;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:29
 * @Description:
 */
@Target( ElementType.TYPE) //注解定义到类上
@Retention( RetentionPolicy.RUNTIME) //生命周期
public @interface Sign {
    String value();
    String message() ;
}
