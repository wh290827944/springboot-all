package com.wh.config.gps.handle;

import com.wh.config.gps.domain.GpsProtocol;
import io.netty.channel.ChannelHandlerContext;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:36
 * @Description:
 */
public interface ISign {

    void handle(ChannelHandlerContext ctx, GpsProtocol gpsProtocol) throws Exception;



}
