package com.wh.config.gps.common;

/**
 * @Author: WangHao
 * @Date: 2021/10/20/10:51
 * @Description:
 */
public interface GpsContant {
    String msId_0100 = "0100";
    String msId_0102 = "0102";
    String msId_0200 = "0200";
    String msId_8001 = "8001";
    String msId_8100 = "8100";
    String msId_0704 = "0704";

    String msId_8104 = "8104";
    String msId_0104 = "0104";
}
