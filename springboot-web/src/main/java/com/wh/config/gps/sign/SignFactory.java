package com.wh.config.gps.sign;

import com.wh.config.gps.domain.GpsProtocol;
import com.wh.config.gps.handle.ISign;
import com.wh.config.gps.utils.SpringUtils;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.Set;

/**
 * @Author: WangHao
 * @Date: 2021/10/21/18:40
 * @Description:
 */
public class SignFactory {


    private static HashMap<String,String> sourceMap = new HashMap<>();

    /**
     * 单例
     *
     */
    static {

        //反射工具包，指明扫描路径
        Reflections reflections = new Reflections("com.wh.config.gps");
        //获取带我们pay注解的类
        Set<Class<?>> classSet =  reflections.getTypesAnnotatedWith(Sign.class);
        //根据注解的值，将全类名放到map中
        for (Class clazz : classSet){
            Sign pay = (Sign) clazz.getAnnotation(Sign.class);
            sourceMap.put(pay.value(),clazz.getCanonicalName());
        }

    }



    public static ISign getInstance(GpsProtocol gpsProtocol){
        ISign chatType = null;
        try {
            //取得全类名
            String className = sourceMap.get(gpsProtocol.getMsgId());
            //取得类对象
            Class  clazz= Class.forName(className);



            //======================创建对象=====================
            chatType  =  (ISign) SpringUtils.getInterface(clazz);

        }catch (Exception e){
            System.err.println("尚未定义处理类标志："+gpsProtocol.getMsgId()+"原始数据为："+gpsProtocol.getRawData());
        }

        return chatType;
    }



}
