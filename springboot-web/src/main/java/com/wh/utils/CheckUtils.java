package com.wh.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUtils {





    public static boolean listIsEmpty(List list) {
        if (list != null && !list.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean listIsNotEmpty(List list) {
        if (list != null && !list.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean mapIsEmpty(Map map) {
        if (map != null && !map.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean mapIsNotEmpty(Map map) {
        if (map != null && !map.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean setIsEmpty(Set set) {
        if (set != null && !set.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean setIsNotEmpty(Set set) {
        if (set != null && !set.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean strIsNotEmpty(String value) {
        if (value != null && value.length() != 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean strIsEmpty(String value) {
        if (value == null || value.length() == 0) {
            return true;
        } else {
            return false;
        }
    }

    public static String checkCommand(String command) {
        Matcher m = Pattern.compile("(\\w+)\\.\\w+\\s+(?i)WHERE").matcher(command);
        if (m.find()) {
            String param = m.group(1);
            if (isNumeric(param.substring(0, 1))) {
                String from = param + ".";
                String to = "q_" + param + ".";
                command = command.replace(from, to);
            }
        }
        return command;
    }

    public static boolean isNumeric(String str) {
        for (int i = str.length(); --i >= 0; ) {
            if (!Character.isDigit(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
