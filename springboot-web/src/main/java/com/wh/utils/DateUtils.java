package com.wh.utils;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateUtils {
    /**
     * 获取某时间段范围内的作业时间列表
     *
     * @param initJobTime 初始作业时间
     * @param type        周期类型:ONCE,MINUTES,HOURS,DAYS,MONTHS,WEEKS
     * @param frequency   执行频率
     * @param delay       延迟时间（分钟）
     * @param startAt     时间范围：开始
     * @param endAt       时间反问：结束
     * @return 时间列表
     */
    public static List<String> listJobTimes(
            String initJobTime, String type, int frequency, int delay, String startAt, String endAt) {
        List<String> jobTimes = new ArrayList<String>();

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        LocalDateTime jobTime = LocalDateTime.parse(initJobTime, df);
        LocalDateTime jobStart = LocalDateTime.parse(startAt, df);
        LocalDateTime jobEnd = LocalDateTime.parse(endAt, df);

        if (jobTime.isAfter(jobEnd)) {
            // 未到时间，不需要计算
            return jobTimes;
        }

        if (delay != 0) {
            // 为便于计算，统一设置延迟delay分钟
            jobTime = jobTime.plusMinutes(delay);
            jobStart = jobStart.plusMinutes(delay);
            jobEnd = jobEnd.plusMinutes(delay);
        }
        // 起始时间
        LocalDateTime realJobTime = jobTime;

        ChronoUnit cronType = null;
        try {
            // 获取对应周期类型
            cronType = ChronoUnit.valueOf(type.toUpperCase());
        } catch (Exception e) {
            System.out.println(e);
        }

        if (cronType == null) {
            // 不在周期列表中，仅执行一次
            if (realJobTime.isAfter(jobStart) && realJobTime.isBefore(jobEnd)) {
                jobTimes.add(df.format(realJobTime));
            }
        } else {
            int i = 1;
            while (realJobTime.isBefore(jobEnd) || realJobTime.isEqual(jobEnd)) {
                if (realJobTime.isAfter(jobStart) || realJobTime.isEqual(jobStart)) {
                    // 当时间迭代至开始时间后，添加至列表
                    jobTimes.add(df.format(realJobTime));
                }
                // 循环
                realJobTime = jobTime.plus(frequency * i++, cronType);
            }
        }

        return jobTimes;
    }

    /**
     * String -> LocalDatetime
     *
     * @param datetime
     * @return
     */
    public static LocalDateTime str2LocalDatetime(String datetime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(datetime, df);
    }

    /**
     * LocalDateTime -> String
     *
     * @param datetime
     * @return
     */
    public static String localDatetime2Str(LocalDateTime datetime) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return datetime.format(df);
    }

    /**
     * String -> Date
     *
     * @param datetime
     * @return
     */
    public static Date str2Datetime(String datetime) {
        return Date.from(str2LocalDatetime(datetime).toInstant(ZoneOffset.ofHours(8)));
    }

    /**
     * Date -> String
     *
     * @param datetime
     * @return
     */
    public static String datetime2Str(Date datetime) {
        return localDatetime2Str(datetime.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }


    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getLocalDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return sdf.format(date);
    }

    public static String getTime(String pattern, Instant instant) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(Date.from(instant));
    }

    /**
     * 获取当前时间
     *
     * @return
     */
    public static String getFormatDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        return sdf.format(date);
    }
}
