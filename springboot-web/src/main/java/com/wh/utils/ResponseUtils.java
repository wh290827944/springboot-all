package com.wh.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializeFilter;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * @param
 * @author zhou.
 * @date 18-8-23 下午3:11
 * @return description:返回状态码，工具类
 */
public class ResponseUtils {
    /**
     * 构建响应结构体
     *
     * @param result  返回结果
     * @param message 结果消息
     * @param code    业务状态码
     * @param status  请求执行状态
     * @return
     */
    public static ResponseEntity<String> responseBody(Object result, String message, Integer code, HttpStatus status) {
        SerializeFilter filter = new ValueFilter() {
            @Override
            public Object process(Object source, String name, Object value) {
                if (null != value && value.getClass().equals(Instant.class)) {
                    LocalDateTime localDateTime = ((Instant) value).atZone(ZoneId.systemDefault()).toLocalDateTime();
                    return DateUtils.localDatetime2Str(localDateTime);
                } else {
                    return value;
                }
            }
        };
        Map<String, Object> response = new HashMap<>();

        if (result != null) {
            response.put("result", result);
        }

        if (message == null) {
            message = "请求成功";
        }
        response.put("msg", message);

        if (code == null) {
            code = 0;
        }
        response.put("code", code);

        if (status == null) {
            status = HttpStatus.OK;
        }
        response.put("status", status.value());

        return new ResponseEntity<>(JSON.toJSONString(response,
                filter,
                SerializerFeature.WriteMapNullValue,
                //将String类型null转成""
                SerializerFeature.WriteNullStringAsEmpty), status);
    }

    public static ResponseEntity<String> success(Object result) {
        return responseBody(result, null, 200, HttpStatus.OK);
    }

    public static ResponseEntity<String> success(String message) {
        return responseBody(null, message, 200, HttpStatus.OK);
    }

    public static ResponseEntity<String> success(Object result, String message) {
        return responseBody(result, message, 200, HttpStatus.OK);
    }

    public static ResponseEntity<String> success(Object result, String message, Integer code) {
        return responseBody(result, message, code, HttpStatus.OK);
    }

    public static ResponseEntity<String> error(String message) {
        return responseBody(null, message, 500, HttpStatus.OK);
    }

    public static ResponseEntity<String> error(Object result, String message) {
        return responseBody(result, message, 500, HttpStatus.OK);
    }

    public static ResponseEntity<String> error(Object result, String message, Integer code) {
        return responseBody(result, message, code, HttpStatus.OK);
    }

    public static ResponseEntity<String> fail(String message) {
        return responseBody(null, message, 500, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<String> fail(Object result, String message) {
        return responseBody(result, message, 500, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static ResponseEntity<String> fail(Object result, String message, Integer code) {
        return responseBody(result, message, code, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
