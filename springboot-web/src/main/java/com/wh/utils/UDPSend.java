package com.wh.utils;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Scanner;

/**
 * @Author: WangHao
 * @Date: 2021/07/28/10:34
 * @Description:
 */
public class UDPSend {

    public static void main(String[] args) throws IOException {
        System.out.println("100000000000ffffffffffffffffffffffffffff".length());
        while (true){
            //接收用户输入的数据
            Scanner sc=new Scanner(System.in);
            String str=sc.nextLine();
            //1.创建数据包对象，封装要发送的数据，接收端的ip，端口号
            byte[] bytes=str.getBytes();
            //创建inetAddress对象封装接收端的地址
            InetAddress inet=InetAddress.getByName("255.255.255.255");
            DatagramPacket dp=new DatagramPacket(bytes, bytes.length,inet,60000);
            //2.创建DatagramSocket对象，数据包的发送对象
            DatagramSocket ds=new DatagramSocket();
            //3.调用ds的send方法，发送数据包
            ds.send(dp);
            //4.释放资源
            ds.close();
        }

    }
}

class UDPRecieve {
    public static void main(String[] args) throws IOException {
        //创建数据包传输对象DatagramSocket绑定端口号
        DatagramSocket ds=new DatagramSocket(60000);
        //创建字节数组来接收发过来的数据
        byte[] bytes=new byte[1024];
        //创建数据包对象DatagramPacket
        DatagramPacket dp=new DatagramPacket(bytes, bytes.length);
        //调用数据包传输对象的receive方法来接收数据
        while (true){
            ds.receive(dp);
            //拆包
            //获取数据长度
            int length=dp.getLength();
            //获取ip地址
            String ip=dp.getAddress().getHostAddress();
            //获取端口号
            int port=dp.getPort();
            System.out.println("ip地址为："+ip+",端口号为："+port+",发送的内容为："+new String(bytes,0,length));
            //释放资源
//            ds.close();
        }

    }
}
