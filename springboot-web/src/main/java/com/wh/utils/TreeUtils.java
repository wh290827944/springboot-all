package com.wh.utils;

import com.wh.entity.Tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @Author: WangHao
 * @Date: 2021/08/24/14:16
 * @Description:
 */
public class TreeUtils {


    public static List<Tree> getTreeList() {
        Tree root = new Tree(0, "根节点");

        Tree _10 = new Tree(10, "10号节点");


        Tree _11 = new Tree(11, "11号节点");
        Tree _111 = new Tree(111, "111号节点");
        Tree _112 = new Tree(112, "112号节点");
        _11.setChildrens(new ArrayList<Tree>() {{
            this.add(_111);
            this.add(_112);
        }});


        Tree _12 = new Tree(12, "12号节点");
        _10.setChildrens(new ArrayList<Tree>() {{
            this.add(_11);
            this.add(_12);
        }});


        Tree _20 = new Tree(20, "20号节点");
        Tree _21 = new Tree(21, "21号节点");
        Tree _22 = new Tree(22, "22号节点");
        _20.setChildrens(new ArrayList<Tree>() {{
            this.add(_21);
            this.add(_22);
        }});

        root.setChildrens(new ArrayList<Tree>() {{
            this.add(_10);
            this.add(_20);
        }});

        return Collections.singletonList(root);
    }

    public static void main(String[] args) {
        List<Tree> rootTree = getTreeList();
        List<Tree> finalChildrenNodes = new ArrayList<>();

        while (rootTree.size()>0){

            List<Tree> tempChildrenList = new ArrayList<>();
            for (Tree tree : rootTree) {
                String name = tree.getName();
                System.out.println(name);
                List<Tree> childrens = tree.getChildrens();
                finalChildrenNodes.add(tree);
                tempChildrenList.addAll(childrens);
                rootTree = tempChildrenList;
                tree.setChildrens(null);
            }
        }


        for (Tree sd : finalChildrenNodes) {
            System.out.println("id:" + sd.getId() + "_______name:" + sd.getName());
        }


    }
}
