package com.wh.rabbitMQ.topic;

import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class TopicConsumer {

    @RabbitListener(bindings = {@QueueBinding(value = @Queue,//创建临时队列
            exchange = @Exchange(value = "topics",type = "topic") ,
            key = {"user.name","user.*"})})
    public void recevivel(String message){
        System.out.println("message1:"+message);
    }

    @RabbitListener(bindings = {@QueueBinding(value = @Queue,//创建临时队列
            exchange = @Exchange(value = "topics",type = "topic") ,
            key = {"order.#","user.#"})})
    public void recevive2(String message){
        System.out.println("message2:"+message);
    }

}
