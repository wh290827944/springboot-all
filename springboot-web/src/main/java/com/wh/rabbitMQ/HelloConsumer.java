package com.wh.rabbitMQ;

import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
//默认值 持久化、非独占、不自动删除
@RabbitListener(queuesToDeclare = @Queue(value = "hello" ,durable = "true",autoDelete = "true") )
public class HelloConsumer {
    @RabbitHandler
    public void recivel(String messagge){
        System.out.println("message =======>"+messagge);
    }
}
