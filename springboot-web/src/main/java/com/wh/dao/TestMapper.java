package com.wh.dao;

import com.wh.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TestMapper {

    User getUserName();
}
