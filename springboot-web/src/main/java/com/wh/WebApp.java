package com.wh;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.amqp.RabbitAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.MultipartAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(scanBasePackages = {"com.wh"}, exclude = {MultipartAutoConfiguration.class, DataSourceAutoConfiguration.class,
        RabbitAutoConfiguration.class})

//@MapperScan( "com.wh.**.dao" )
@EnableAsync
@EnableTransactionManagement
public class WebApp  extends SpringBootServletInitializer {

    public static final Logger logger = LoggerFactory.getLogger(WebApp.class);

    public static void main(String[] args) {
        SpringApplication.run(WebApp.class,args);
        logger.info("SpringBoot>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>{}", "startSuccess");
    }

    @Override
    protected SpringApplicationBuilder configure(
            SpringApplicationBuilder builder) {
        return builder.sources(this.getClass());
    }
}
