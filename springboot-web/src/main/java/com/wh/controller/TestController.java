package com.wh.controller;


import com.wh.common.response.Response;
import com.wh.common.response.ResponseHttpCode;
import com.wh.entity.TestEntity;
import com.wh.entity.TestEntity2;
import com.wh.entity.User;
import com.wh.service.ITestService;
import com.wh.service.impl.TestServiceImpl;
import com.wh.utils.SpringUtils;
import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 * 测试接口
 * @description 系统级别
 */
@RequestMapping( "/test" )
@RestController
public class TestController {
    @Autowired
    private ITestService testService;

    @Autowired
    private TestServiceImpl testServiceImpl;

    @Value("${wh}")
   private String wh;
    @RequestMapping( value = "/wh", method = RequestMethod.GET )
    public Response get() {

        return new Response(ResponseHttpCode.OK, wh);
    }

    @RequestMapping( value = "/getMessage", method = RequestMethod.GET )
    public Response getMessage() {
        String message = testServiceImpl.getMessage();

        return new Response(ResponseHttpCode.OK, message);
    }



    @RequestMapping( value = "/wh", method = RequestMethod.POST )
    public Response wh(TestEntity testEntity, TestEntity2 ab, @RequestParam("a") String a, String b) {

        return new Response(ResponseHttpCode.OK, "成功");
    }



//
//
//    @RequestMapping( "/chatType" )
//    public Response<User> get(int type) {
//        ChatType instance = ChatFactory.getInstance(type);
//        instance.say();
//        return new Response(ResponseHttpCode.OK, "成功");
//    }


    /**
     * 列表
     * @param  id 用户id
     * @param a 测试字段
     * @return
     */
    @RequestMapping( "/hello" )
    public Response<User> test(@RequestParam String id,@RequestParam String a) {
        User userName = testService.getUserName();
        User userName1 = SpringUtils.testServiceStatic.getUserName();

        ITestService anInterface = SpringUtils.getInterface(ITestService.class);
        User userName2 = anInterface.getUserName();

        return new Response(ResponseHttpCode.OK, userName2);
    }

    /**
     * 列表2
     * @param user 用户名
     * @return
     */
    @RequestMapping( "/get" )
    public Response<User> get(@RequestBody User user) {
        User userName = testService.getUserName();
        User userName1 = SpringUtils.testServiceStatic.getUserName();

        ITestService anInterface = SpringUtils.getInterface(ITestService.class);
        User userName2 = anInterface.getUserName();

        return new Response(ResponseHttpCode.OK, userName2);
    }

    public static void main(String[] args) {
        DocsConfig config = new DocsConfig();
        config.setProjectPath("E:\\WH\\springboot-all"); // 项目根目录
        config.setProjectName("springboot-all"); // 项目名称
        config.setApiVersion("V1.0");       // 声明该API的版本
        config.setDocsPath("E:\\WH\\springboot-all\\log"); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        Docs.buildHtmlDocs(config); // 执行生成文档
    }
}
