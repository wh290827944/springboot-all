package com.wh.controller;

import com.wh.config.ServerConfig;
import com.wh.config.fileupload.ProgressEntity;
import com.wh.config.fileupload.UploadConstant;
import com.wh.entity.IotFileDO;
import com.wh.utils.ResponseUtils;
import com.wh.utils.SnowflakeIdWorker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

@RestController
@RequestMapping(value = {"/api/iot/files", "/space-*/api/iot/gis"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Slf4j
@CrossOrigin
public class FileController {
    @Autowired
    ServerConfig serverConfig;


    /**
     * 上传前需清空，然后才能获取到session中的进度条
     * @param request
     * @Author: WangHao
     */
    @RequestMapping(value = "/uploadReset", method = RequestMethod.GET)
    public String uploadReset( HttpServletRequest request
    ) throws Exception {
        HttpSession session = request.getSession();
        session.setAttribute("progressEntity",null);


        return "重置成功";
    }

    /**
     * 获取上传进度
     *
     * @return
     */
    @GetMapping(value = "/uploadStatus")
    public Object uploadStatus(HttpServletRequest request) {
        HttpSession session = request.getSession();
        ProgressEntity percent = (ProgressEntity) session.getAttribute("progressEntity");
        // System.out.println("^^^^"+percent.toString());
        //当前时间
        long nowTime = System.currentTimeMillis();
        //已传输的时间
        long time = (nowTime - percent.getStartTime())/1000+1;
        //传输速度 ;单位：byte/秒
        double velocity =((double) percent.getBytesRead())/(double)time;
        //估计总时间
        double totalTime = percent.getContentLength()/velocity;
        //估计剩余时间
        double timeLeft = totalTime - time;
        //已经完成的百分比
        int percent1 = (int)(100 * (double) percent.getBytesRead() / (double) percent.getContentLength());
        //已经完成数单位:m
        double length = ((double) percent.getBytesRead())/1024/1024;
        //总长度  M
        double totalLength = (double) percent.getContentLength()/1024/1024;
        StringBuffer sb = new StringBuffer();
        //拼接上传信息传递到jsp中
        sb.append(percent+":"+length+":"+totalLength+":"+velocity+":"+time+":"+totalTime+":"+timeLeft+":"+percent.getItems());
        System.out.println("百分比====>"+percent1);
        System.out.println("已传输时间====>"+time+"------剩余时间--->"+timeLeft+"======已经完成===="+length);
        return "当前文件:"+percent.getItems()+"=====>百分比====>"+percent1+"====>已传输时间====>"+time+"------剩余时间--->"+timeLeft+"======已经完成===="+length;
    }


    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<String> uploadFile(
            @RequestParam(value = "file", required = false) MultipartFile file,
            @RequestParam Integer code
    ) throws Exception {


        if (file == null || file.isEmpty()) {
            return null;
        }

        byte[] bytes = file.getBytes();;

        // 生成jpg图片
        File filePath = new File(UploadConstant.equipmentTwinPath);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }

        OutputStream out = null;
        String oriName =  file.getOriginalFilename();
        String suffix = file.getOriginalFilename().substring(oriName.lastIndexOf("."));
        String   onlyName = SnowflakeIdWorker.get18UniqueNumbers() + suffix;
        Long   size = file.getSize();
        File  fileO = new File(filePath, onlyName);
        try {
//            file = File.createTempFile("thumbnail", ".jpg", filePath);

            file.transferTo(fileO);
//            out = new FileOutputStream(file);
//            out.write(bytes);
//            out.flush();
        } catch (Exception e) {

            log.error("上传文件失败：{}",e.getMessage());
            throw new Exception("文件上传失败!");
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }

        String fileUrl = serverConfig.getIpAndPort() + UploadConstant.resource + onlyName;
        IotFileDO iotFileDO = new IotFileDO();
        iotFileDO.setFileName(oriName);
        iotFileDO.setFileOnlyname(onlyName);
        iotFileDO.setFileUrl(fileUrl);
        iotFileDO.setFileSize(size);
        iotFileDO.setFileStatus(1);
        iotFileDO.setFileSuffix(suffix);

        return ResponseUtils.success(iotFileDO);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ResponseEntity<String> delete(
            @RequestParam String fullPath
    ) throws Exception {

        File file = new File(fullPath);
        if (file.exists() == true && file.isFile() == true) {
            file.delete();

        }



        return ResponseUtils.success("删除成功");
    }


    @RequestMapping("/resources/equipmentTwin/{fileName}")
    public void downLoad(@PathVariable("fileName") String fileName,  HttpServletResponse response) {
        System.err.println("开始下载文件");


        File file = new File(UploadConstant.equipmentTwinPath, fileName);
        if (file.exists()) {
            response.setContentType("application/octet-stream");//
            response.setHeader("content-type", " model/gltf-binary");
            response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);// 设置文件名
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
                System.out.println("success");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }


    }




}
