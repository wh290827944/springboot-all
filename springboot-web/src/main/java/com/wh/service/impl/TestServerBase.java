package com.wh.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: WangHao
 * @Date: 2021/09/09/17:33
 * @Description:
 */
public class TestServerBase {
    @Autowired
    private TestOtherServer testOtherServer;

    public String getMessage(){
        return testOtherServer.getMessage();
    }

}
