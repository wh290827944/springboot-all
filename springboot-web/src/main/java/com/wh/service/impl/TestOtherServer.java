package com.wh.service.impl;

import org.springframework.stereotype.Service;

/**
 * @Author: WangHao
 * @Date: 2021/09/09/17:34
 * @Description:
 */
@Service
public class TestOtherServer {

    public String getMessage(){
        return "wanghao";
    }
}
