CREATE TABLE `analysis_model`
(
    `id`                      int(11) unsigned NOT NULL AUTO_INCREMENT,
    `name`                    varchar(20)   NOT NULL DEFAULT '',
    `device_Type`             varchar(20)   NOT NULL DEFAULT '',
    `hazard_source_level`     varchar(20)   NOT NULL DEFAULT '',
    `key_dangerous_chemicals` varchar(20)   NOT NULL DEFAULT '',
    `key_technologies`        varchar(50)   NOT NULL DEFAULT '',
    `industry_norms`          varchar(50)   NOT NULL DEFAULT '',
    `model_config`            text          DEFAULT NULL,
    `container_id`            varchar(100)  NOT NULL DEFAULT '',
    `create_at`               datetime      DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;